xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.globallogic.com/Bootcamp/SC/ParameterManager/Get";
(:: import schema at "../XSD/Get_ParameterManager.xsd" ::)
declare namespace ns2="http://xmlns.oracle.com/pcbpel/adapter/db/sp/GetParameterManagerDBAdapter";
(:: import schema at "../JCA/GetParameterManagerDBAdapter/GetParameterManagerDBAdapter_sp.xsd" ::)

declare variable $GetREQ as element() (:: schema-element(ns1:GetREQ) ::) external;

declare function local:get_GetParameterManagerDBAdapterREQ($GetREQ as element() (:: schema-element(ns1:GetREQ) ::)) as element() (:: schema-element(ns2:InputParameters) ::) {
    <ns2:InputParameters>
        <ns2:P_KEY>{fn:data($GetREQ/ns1:key)}</ns2:P_KEY>
    </ns2:InputParameters>
};

local:get_GetParameterManagerDBAdapterREQ($GetREQ)
