xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://www.globallogic.com/Bootcamp/SC/ParameterManager/Get";
(:: import schema at "../XSD/Get_ParameterManager.xsd" ::)
declare namespace ns1="http://xmlns.oracle.com/pcbpel/adapter/db/sp/GetParameterManagerDBAdapter";
(:: import schema at "../JCA/GetParameterManagerDBAdapter/GetParameterManagerDBAdapter_sp.xsd" ::)

declare namespace ns3 = "http://www.globallogic.com/Bootcamp/Support/Result/v1";

declare variable $OutputParameters as element() (:: schema-element(ns1:OutputParameters) ::) external;

declare function local:get_GetRSP($OutputParameters as element() (:: schema-element(ns1:OutputParameters) ::)) as element() (:: schema-element(ns2:GetRSP) ::) {
    <ns2:GetRSP>
        <ns3:Result status="{
                            if (fn:data($OutputParameters/ns1:P_ERROR/ns1:CODE_ ) = '00')
                            then 'OK'
                            else 'ERROR'}">
            {
                if ($OutputParameters/ns1:P_ERROR/ns1:DESCRIPTION_)
                then attribute description {fn:data($OutputParameters/ns1:P_ERROR/ns1:DESCRIPTION_)}
                else ()
            }
        </ns3:Result>
        {
            if ($OutputParameters/ns1:P_VALUE)
            then <ns2:value>{fn:data($OutputParameters/ns1:P_VALUE)}</ns2:value>
            else ()
        }
    </ns2:GetRSP>
};

local:get_GetRSP($OutputParameters)
