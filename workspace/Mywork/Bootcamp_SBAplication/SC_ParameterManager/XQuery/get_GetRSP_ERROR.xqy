xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://www.globallogic.com/Bootcamp/SC/ParameterManager/Get";
(:: import schema at "../XSD/Get_ParameterManager.xsd" ::)
declare namespace ns1="http://www.globallogic.com/Bootcamp/Support/Result/v1";
(:: import schema at "../../Commons/XSD/Support/Result_v1.xsd" ::)

declare variable $Result as element() (:: schema-element(ns1:Result) ::) external;

declare function local:get_GetRSP_ERROR($Result as element() (:: schema-element(ns1:Result) ::)) as element() (:: schema-element(ns2:GetRSP) ::) {
    <ns2:GetRSP>
        {$Result}
    </ns2:GetRSP>
};

local:get_GetRSP_ERROR($Result)
