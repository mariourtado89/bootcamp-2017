xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://www.globallogic.com/Bootcamp/Support/MessageHeader/v1";
(:: import schema at "../../Commons/XSD/Support/MessageHeader_v1.xsd" ::)
declare namespace ns1="http://www.globallogic.com/ES/Venta/Registrar";
(:: import schema at "../XSD/Registrar_Venta.xsd" ::)

declare variable $MessageHeader as element() (:: schema-element(ns2:MessageHeader) ::) external;

declare function local:get_Registrar_Venta_FRSP($MessageHeader as element() (:: schema-element(ns2:MessageHeader) ::)) as element() (:: schema-element(ns1:Registrar_Venta_FRSP) ::) {
    <ns1:Registrar_Venta_FRSP>
      {$MessageHeader}
    </ns1:Registrar_Venta_FRSP>
};

local:get_Registrar_Venta_FRSP($MessageHeader)
