xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.globallogic.com/Bootcamp/SC/TransactionManager/UpdateStatus";
(:: import schema at "../XSD/UpdateStatus_TransactionManager.xsd" ::)
declare namespace ns2="http://xmlns.oracle.com/pcbpel/adapter/db/sp/UpdateStatusTransactionManagerDBAdapter";
(:: import schema at "../JCA/UpdaStatusTransactionManagerDBAdapter/UpdateStatusTransactionManagerDBAdapter_sp.xsd" ::)

declare namespace ns3 = "http://www.globallogic.com/Bootcamp/Support/MessageHeader/v1";

declare variable $UpdateStatusREQ as element() (:: schema-element(ns1:UpdateStatusREQ) ::) external;

declare function local:get_UpdateStatusAdapterREQ($UpdateStatusREQ as element() (:: schema-element(ns1:UpdateStatusREQ) ::)) as element() (:: schema-element(ns2:InputParameters) ::) {
    <ns2:InputParameters>
        <ns2:P_UUID>{fn:data($UpdateStatusREQ/ns3:MessageHeader/ns3:Trace/@uuid)}</ns2:P_UUID>
        <ns2:P_STATUS>{fn:data($UpdateStatusREQ/ns1:Status)}</ns2:P_STATUS>
    </ns2:InputParameters>
};

local:get_UpdateStatusAdapterREQ($UpdateStatusREQ)
