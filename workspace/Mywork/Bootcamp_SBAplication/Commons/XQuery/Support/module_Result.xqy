xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

module namespace ns2="http://www.globallogic.com/Bootcamp/Support/Result/xquery/module/v1";

declare namespace ns1="http://www.globallogic.com/Bootcamp/Support/Result/v1";
(:: import schema at "../../XSD/Support/Result_v1.xsd" ::)

declare function ns2:get_SimpleResult($status as xs:string, 
                                        $description as xs:string ?) 
                                        as element() (:: schema-element(ns1:Result) ::) {
    <ns1:Result status="{fn:data($status)}">
      {
        if($description)
        then attribute description {fn:data($description)}
         else ()
      }
    </ns1:Result>
};



declare function ns2:get_ResultError_wSourceFault($description as xs:string?, 
                            $SourceFault as element()?) 
                            as element() (:: schema-element(ns1:Result) ::) {
    <ns1:Result status="ERROR">
     {
        if($description)
        then attribute description {fn:data($description)}
         else ()
      }
      <ns1:SourceFault>{$SourceFault}</ns1:SourceFault>
    </ns1:Result>
};