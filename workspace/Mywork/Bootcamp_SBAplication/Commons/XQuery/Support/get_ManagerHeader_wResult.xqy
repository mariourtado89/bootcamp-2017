xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.globallogic.com/Bootcamp/Support/MessageHeader/v1";
(:: import schema at "../../XSD/Support/MessageHeader_v1.xsd" ::)
declare namespace ns2="http://www.globallogic.com/Bootcamp/Support/Result/v1";
(:: import schema at "../../XSD/Support/Result_v1.xsd" ::)

declare variable $MessageHeader as element() (:: schema-element(ns1:MessageHeader) ::) external;
declare variable $Result as element() (:: schema-element(ns2:Result) ::) external;

declare function local:func($MessageHeader as element() (:: schema-element(ns1:MessageHeader) ::), 
                            $Result as element() (:: schema-element(ns2:Result) ::)) 
                            as element() (:: schema-element(ns1:MessageHeader) ::) {
    <ns1:MessageHeader>
        {$MessageHeader/*[1]}
        {$Result}
    </ns1:MessageHeader>
};

local:func($MessageHeader, $Result)
