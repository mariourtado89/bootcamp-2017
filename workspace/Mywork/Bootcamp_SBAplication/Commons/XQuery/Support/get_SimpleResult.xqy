xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.globallogic.com/Bootcamp/Support/Result/v1";
(:: import schema at "../../XSD/Support/Result_v1.xsd" ::)

declare variable $status as xs:string external;
declare variable $description as xs:string external;

declare function local:get_SimpleResult($status as xs:string, 
                                        $description as xs:string ?) 
                                        as element() (:: schema-element(ns1:Result) ::) {
    <ns1:Result status="{fn:data($status)}">
      {
        if($description)
        then attribute description {fn:data($description)}
         else ()
      }
    </ns1:Result>
};

local:get_SimpleResult($status, $description)
