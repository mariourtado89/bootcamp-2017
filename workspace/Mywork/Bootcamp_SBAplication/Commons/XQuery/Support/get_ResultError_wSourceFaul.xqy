xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.globallogic.com/Bootcamp/Support/Result/v1";
(:: import schema at "../../XSD/Support/Result_v1.xsd" ::)

declare variable $description as xs:string external;
declare variable $SourceFault as element() external;

declare function local:func($description as xs:string?, 
                            $SourceFault as element()?) 
                            as element() (:: schema-element(ns1:Result) ::) {
    <ns1:Result status="ERROR">
     {
        if($description)
        then attribute description {fn:data($description)}
         else ()
      }
      <ns1:SourceFault>{$SourceFault}</ns1:SourceFault>
    </ns1:Result>
};

local:func($description, $SourceFault)
