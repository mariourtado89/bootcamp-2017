xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.globallogic.com/Bootcamp/Support/MessageHeader/v1";
(:: import schema at "../../XSD/Support/MessageHeader_v1.xsd" ::)

declare variable $service as xs:string external;
declare variable $operation as xs:string external;
declare variable $MessageHeader as element() (:: schema-element(ns1:MessageHeader) ::) external;

declare function local:get_MessageHeader($service as xs:string, 
                                         $operation as xs:string, 
                                         $MessageHeader as element()? (:: schema-element(ns1:MessageHeader) ::)) as element() (:: schema-element(ns1:MessageHeader) ::) {
    <ns1:MessageHeader>
        <ns1:Trace uuid="{ if ( data($MessageHeader/ns1:Trace/@uuid) = '') then fn-bea:uuid() else data($MessageHeader/ns1:Trace/@uuid) }" reqTime="{fn:current-dateTime()}">
            {
                if ($MessageHeader/ns1:Trace/@rspTime)
                then attribute rspTime {fn:data($MessageHeader/ns1:Trace/@rspTime)}
                else ()
            }
            <ns1:Service name="{$service}" operation="{$operation}" />
        </ns1:Trace>
    </ns1:MessageHeader>
};

local:get_MessageHeader($service, $operation, $MessageHeader)
