xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.globallogic.com/Bootcamp/SC/TransactionManager/UpdateStatus";
(:: import schema at "../../../../SC_TransactionManager/XSD/UpdateStatus_TransactionManager.xsd" ::)
declare namespace ns2="http://www.globallogic.com/Bootcamp/Support/MessageHeader/v1";
(:: import schema at "../../../XSD/Support/MessageHeader_v1.xsd" ::)

declare variable $MessageHeader as element() (:: schema-element(ns2:MessageHeader) ::) external;
declare variable $status as xs:string external;

declare function local:get_UpdateStatusREQ($MessageHeader as element() (:: schema-element(ns2:MessageHeader) ::), 
                                           $status as xs:string) 
                                           as element() (:: schema-element(ns1:UpdateStatusREQ) ::) {
    <ns1:UpdateStatusREQ>
      {$MessageHeader}
        <ns1:Status>{fn:data($status)}</ns1:Status>
    </ns1:UpdateStatusREQ>
};

local:get_UpdateStatusREQ($MessageHeader, $status)
