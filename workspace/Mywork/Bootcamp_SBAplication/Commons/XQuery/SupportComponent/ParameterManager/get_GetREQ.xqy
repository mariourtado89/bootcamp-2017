xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.globallogic.com/Bootcamp/SC/ParameterManager/Get";
(:: import schema at "../../../../SC_ParameterManager/XSD/Get_ParameterManager.xsd" ::)

declare variable $key as xs:string external;

declare function local:func($key as xs:string) as element() (:: schema-element(ns1:GetREQ) ::) {
    <ns1:GetREQ>
        <ns1:key>{$key}</ns1:key>
    </ns1:GetREQ>
};

local:func($key)
