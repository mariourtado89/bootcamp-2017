xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.globallogic.com/Bootcamp/SC/LoggerManager/Log";
(:: import schema at "../../../../SC_LoggerManager/XSD/Log_LoggerManager.xsd" ::)
declare namespace ns2="http://www.globallogic.com/Bootcamp/Support/MessageHeader/v1";
(:: import schema at "../../../XSD/Support/MessageHeader_v1.xsd" ::)

declare variable $MessageHeader as element() (:: schema-element(ns2:MessageHeader) ::) external;
declare variable $stage as xs:string external;
declare variable $component as xs:string external;
declare variable $Message as element() external;

declare function local:get_LogREQ($MessageHeader as element() (:: schema-element(ns2:MessageHeader) ::), 
                                  $stage as xs:string, 
                                  $component as xs:string, 
                                  $Message as element()) 
                                  as element() (:: schema-element(ns1:LogREQ) ::) {
    <ns1:LogREQ>
        {$MessageHeader}
        <ns1:Place stage="{fn:data($stage)}" component="{fn:data($component)}">
        </ns1:Place>
        <ns1:Payload>{$Message}</ns1:Payload>
    </ns1:LogREQ>
};

local:get_LogREQ($MessageHeader, $stage, $component, $Message)
