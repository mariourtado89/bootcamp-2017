xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.globallogic.com/Bootcamp/SC/LoggerManager/Log";
(:: import schema at "../XSD/Log_LoggerManager.xsd" ::)
declare namespace ns2="http://xmlns.oracle.com/pcbpel/adapter/db/sp/LogLoggerManagerDBAdapter";
(:: import schema at "../JCA/LogLoggerManagerDBAdapter/LogLoggerManagerDBAdapter_sp.xsd" ::)

declare namespace ns3 = "http://www.globallogic.com/Bootcamp/Support/MessageHeader/v1";

declare variable $LogREQ as element() (:: schema-element(ns1:LogREQ) ::) external;

declare function local:get_LogLoggerManagerDBAdapter($LogREQ as element() (:: schema-element(ns1:LogREQ) ::)) as element() (:: schema-element(ns2:InputParameters) ::) {
    <ns2:InputParameters>
        <ns2:P_UUID>{fn:data($LogREQ/ns3:MessageHeader/ns3:Trace/@uuid)}</ns2:P_UUID>
        <ns2:P_SERVICE_NAME>{fn:data($LogREQ/ns3:MessageHeader/ns3:Trace/ns3:Service/@name)}</ns2:P_SERVICE_NAME>
        <ns2:P_CAPABILITY_NAME>{fn:data($LogREQ/ns3:MessageHeader/ns3:Trace/ns3:Service/@operation)}</ns2:P_CAPABILITY_NAME>
        <ns2:P_COMPONENT>{fn:data($LogREQ/ns1:Place/@component)}</ns2:P_COMPONENT>
        <ns2:P_STAGE>{fn:data($LogREQ/ns1:Place/@stage)}</ns2:P_STAGE>
        <ns2:P_MESSAGE>{fn-bea:serialize($LogREQ/ns1:Payload/*)}</ns2:P_MESSAGE>
    </ns2:InputParameters>
};

local:get_LogLoggerManagerDBAdapter($LogREQ)
